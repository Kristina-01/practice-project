import os
import ast

def copy_array(src_array, dest_array, start_position):
    if start_position + len(src_array) > len(dest_array):
        print("Недостаточно места в dest_array для копирования данных.")
        return

    for i in range(len(src_array)):
        dest_array[start_position + i] = src_array[i]

if __name__ == "__main__":
    input_source_array_str = os.environ.get("SOURCE_ARRAY")
    start_position = int(os.environ.get("START_POSITION", 0))
    input_destination_array_str = os.environ.get("DEST_ARRAY")

    source_array = ast.literal_eval(input_source_array_str) if input_source_array_str else []
    destination_array = ast.literal_eval(input_destination_array_str) if input_destination_array_str else []

    copy_array(source_array, destination_array, start_position)

    print(destination_array)
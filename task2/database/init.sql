CREATE TABLE students(
  id serial PRIMARY KEY,
  name text NOT NULL,
  surname text NOT NULL,
  date_birthday DATE
);


CREATE TABLE subjects(
  id serial PRIMARY KEY,
  subject_name varchar(100) NOT NULL
);

CREATE TABLE grades(
  id serial PRIMARY KEY,
  id_subjects int REFERENCES subjects NOT NULL,
  id_students int REFERENCES students NOT NULL,
  grade smallint NOT NULL
);

INSERT INTO students(name, surname, date_birthday) VALUES
('Иван', 'Иванов', '2000-03-02'),
('Екатерина', 'Смирнова', '2001-07-22'),
('Алексей', 'Петров', '2000-11-10'),
('Мария', 'Козлова', '2000-04-30'),
('Дмитрий', 'Демин', '2001-09-18');

INSERT INTO subjects(subject_name) VALUES
('Инструментальный анализ защищенности'),
('Модели развертывания удаленных сервисов'),
('Организация оброботки больших данных'),
('Системы ведения хранилищ данных'),
('Алгоритмы численных методов'),
('Исследование программного кода'),
('Компьютерная экспертиза');

INSERT INTO grades(id_subjects, id_students, grade)
VALUES (1, 1, 5), 
     (1, 2, 5), 
     (1, 3, 5),
     (1, 4, 5),
     (1, 5, 5),
     (2, 1, 4), 
     (2, 2, 5), 
     (2, 3, 5),
     (2, 4, 5),
     (2, 5, 4),
     (3, 1, 5), 
     (3, 2, 5), 
     (3, 3, 5),
     (3, 4, 4),
     (3, 5, 4),
     (4, 1, 5), 
     (4, 2, 5), 
     (4, 3, 4),
     (4, 4, 5),
     (4, 5, 4),
     (5, 1, 5), 
     (5, 2, 5), 
     (5, 3, 3),
     (5, 4, 5),
     (5, 5, 4),
     (6, 1, 3), 
     (6, 2, 3), 
     (6, 3, 3),
     (6, 4, 5),
     (6, 5, 4),
     (7, 1, 4), 
     (7, 2, 4), 
     (7, 3, 5),
     (7, 4, 4),
     (7, 5, 4);

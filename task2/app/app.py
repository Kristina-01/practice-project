import os
import psycopg2
from psycopg2 import sql
from tabulate import tabulate

url = os.environ.get('POSTGRES_URI')

def execute_query():
    try:
        connection = psycopg2.connect(url)
        query = sql.SQL("""SELECT subject_name, COUNT(*)
                        FROM grades g1
                        LEFT JOIN subjects s ON s.id = g1.id_subjects
                        WHERE grade = 5
                        GROUP BY subject_name;""")
        
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        
        if result:
            headers = ["Subject Name", "Count"]
            print(tabulate(result, headers=headers, tablefmt="grid"))
        else:
            print("No results.")

        connection.close()
    except Exception as e:
        print(f"PostgreSQL connect: Failed: Exception: {e}")

execute_query()